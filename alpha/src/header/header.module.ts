import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material';

import { HeaderComponent } from './header.component';

@NgModule({
  declarations: [HeaderComponent],
  imports: [
    FlexLayoutModule,
    MatButtonModule
  ],
  exports: [HeaderComponent]
})
export class HeaderModule { }
