import {Component, OnDestroy, OnInit} from "@angular/core";
import {User} from "../shared/user";
import {MatSnackBar} from "@angular/material";
import {AuthService} from "../services/auth.service";
import {Observable, Subscription} from "rxjs";
import {Credentials} from '../shared/credentials';
import {Router} from '@angular/router';

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["../shared/styles/login.scss"]
})

export class LoginComponent implements OnDestroy, OnInit {
  private readonly snackBar: MatSnackBar;
  private readonly authService: AuthService;
  private readonly router: Router;

  public loggedInUser$: Observable<User>;
  public credentials: Credentials = new Credentials();
  private loginSubscription: Subscription;

  constructor(snackBar: MatSnackBar, authService: AuthService, router: Router) {
    this.snackBar = snackBar;
    this.authService = authService;
    this.router = router;
  }

  ngOnInit(): void {
    this.loggedInUser$ = this.authService.getUser();
  }

  ngOnDestroy(): void {
    this.loginSubscription && this.loginSubscription.unsubscribe();
  }

  public login(): void {
    this.loginSubscription = this.authService.login(this.credentials).subscribe(() => {
      this.router.navigate(["users"]);
    }, (error: string) => {
      this.snackBar.open("Failed to login user!", undefined, {
        duration: 3000,
        verticalPosition: "top"
      });
    });
  }
}